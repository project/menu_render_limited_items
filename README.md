# Menu render limited items

Menu render limited items provides a replacement for
core's Menu block that filters out the menu links based on the limit set 
in the block. The limitation is only for menu level one. Set 0 to display
 unlimited items.

## Usage
In order for this module to have any effect, you must replace menu blocks
provided by the System module with menu blocks provided by this module.

Render limit can be configured on the "Configure block" page.

## Installation
`composer require drupal/menu_render_limited_items`.
